use common::{fdsm_glyph_internal, msdfgen_glyph_internal, INTER};
use fdsm::{
    bezier::prepared::PreparedColoredShape,
    correct_error::ErrorCorrectionConfig,
    shape::{ColoredContour, Shape},
};
use iai_callgrind::library_benchmark;
use image::Rgb32FImage;
use ttf_parser::{Face, GlyphId};
mod common;

type TF = (Rgb32FImage, Shape<ColoredContour>, PreparedColoredShape);
type TM = (
    msdfgen::Bitmap<msdfgen::Rgb<f32>>,
    msdfgen::Shape,
    msdfgen::Framing<f64>,
);

fn setup_ec_fdsm(font: &[u8]) -> Vec<TF> {
    let font = Face::parse(font, 0).unwrap();
    let present_ids = (0..font.number_of_glyphs())
        .filter_map(|i| {
            let glyph_id = GlyphId(i);
            font.glyph_bounding_box(glyph_id)
                .is_some()
                .then_some(glyph_id)
        })
        .take(5)
        .collect::<Vec<_>>();
    present_ids
        .iter()
        .map(|id| fdsm_glyph_internal(&font, *id, 16.0))
        .collect::<Vec<_>>()
}

#[library_benchmark]
#[bench::noto(setup_ec_fdsm(notosans::REGULAR_TTF))]
#[bench::inter(setup_ec_fdsm(INTER))]
fn iai_benchmark_ec_with_fdsm(input: Vec<TF>) -> Vec<Rgb32FImage> {
    input
        .into_iter()
        .map(|(mut bitmap, shape, prepared_shape)| {
            fdsm::correct_error::correct_error_msdf(
                &mut bitmap,
                &shape,
                &prepared_shape,
                4.0,
                &ErrorCorrectionConfig::default(),
            );
            bitmap
        })
        .collect()
}

fn setup_ec_msdfgen(font: &[u8]) -> Vec<TM> {
    let font = Face::parse(font, 0).unwrap();
    let present_ids = (0..font.number_of_glyphs())
        .filter_map(|i| {
            let glyph_id = GlyphId(i);
            font.glyph_bounding_box(glyph_id)
                .is_some()
                .then_some(glyph_id)
        })
        .take(5)
        .collect::<Vec<_>>();
    present_ids
        .iter()
        .map(|id| msdfgen_glyph_internal(&font, *id, 16.0))
        .collect::<Vec<_>>()
}

#[library_benchmark]
#[bench::noto(setup_ec_msdfgen(notosans::REGULAR_TTF))]
#[bench::inter(setup_ec_msdfgen(INTER))]
fn iai_benchmark_ec_with_msdfgen(input: Vec<TM>) -> Vec<msdfgen::Bitmap<msdfgen::Rgb<f32>>> {
    input
        .into_iter()
        .map(|(mut bitmap, shape, framing)| {
            shape.correct_msdf_error(
                &mut bitmap,
                framing,
                msdfgen::MsdfGeneratorConfig::default(),
            );
            bitmap
        })
        .collect()
}

iai_callgrind::library_benchmark_group!(
    name = bench_ec_group;
    benchmarks =
        iai_benchmark_ec_with_fdsm,
        iai_benchmark_ec_with_msdfgen
);

iai_callgrind::main!(library_benchmark_groups = bench_ec_group);
