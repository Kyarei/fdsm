use common::INTER;
use iai_callgrind::{black_box, library_benchmark};
use image::RgbImage;
use ttf_parser::{Face, GlyphId};
mod common;

fn iai_benchmark_with<T>(callback: impl Fn(&Face, GlyphId, f64) -> T, font: &[u8]) -> Vec<T> {
    let font = Face::parse(font, 0).unwrap();
    let present_ids = (0..font.number_of_glyphs())
        .filter_map(|i| {
            let glyph_id = GlyphId(i);
            font.glyph_bounding_box(glyph_id)
                .is_some()
                .then_some(glyph_id)
        })
        .take(100)
        .collect::<Vec<_>>();
    present_ids
        .iter()
        .map(|id| callback(&font, black_box(*id), 16.0))
        .collect::<Vec<_>>()
}

#[library_benchmark]
fn iai_benchmark_noto_with_fdsm() -> Vec<RgbImage> {
    iai_benchmark_with(common::fdsm_glyph, notosans::REGULAR_TTF)
}

#[library_benchmark]
fn iai_benchmark_noto_with_msdfgen() -> Vec<msdfgen::Bitmap<msdfgen::Rgb<u8>>> {
    iai_benchmark_with(common::msdfgen_glyph, notosans::REGULAR_TTF)
}

#[library_benchmark]
fn iai_benchmark_inter_with_fdsm() -> Vec<RgbImage> {
    iai_benchmark_with(common::fdsm_glyph, INTER)
}

#[library_benchmark]
fn iai_benchmark_inter_with_msdfgen() -> Vec<msdfgen::Bitmap<msdfgen::Rgb<u8>>> {
    iai_benchmark_with(common::msdfgen_glyph, INTER)
}

iai_callgrind::library_benchmark_group!(
    name = bench_msdf_group;
    benchmarks =
        iai_benchmark_noto_with_fdsm,
        iai_benchmark_noto_with_msdfgen,
        iai_benchmark_inter_with_fdsm,
        iai_benchmark_inter_with_msdfgen
);

iai_callgrind::main!(library_benchmark_groups = bench_msdf_group);
