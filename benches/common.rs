use fdsm::bezier::prepared::PreparedColoredShape;
use fdsm::correct_error::correct_error_msdf;
use fdsm::correct_error::ErrorCorrectionConfig;
use fdsm::generate::generate_msdf;
use fdsm::shape::ColoredContour;
use fdsm::transform::Transform;
use image::ImageBuffer;
use image::Pixel;
use image::Primitive;
use image::Rgb;
use image::Rgb32FImage;
use na::Similarity2;
use na::Vector2;
use nalgebra as na;

use nalgebra::Affine2;

use fdsm::shape::Shape;

use image::RgbImage;

use ttf_parser::GlyphId;

use ttf_parser::Face;

pub fn msdfgen_glyph_internal(
    face: &Face,
    glyph_id: GlyphId,
    shrinkage: f64,
) -> (
    msdfgen::Bitmap<msdfgen::Rgb<f32>>,
    msdfgen::Shape,
    msdfgen::Framing<f64>,
) {
    let bbox = face.glyph_bounding_box(glyph_id).unwrap();
    let mut shape = msdf_import_from_ttf_parser::glyph_shape_vendored(face, glyph_id).unwrap();
    shape.edge_coloring_simple(0.03, 69441337420);
    const RANGE: f64 = 4.0;
    let framing = msdfgen::Framing {
        projection: msdfgen::Projection {
            scale: msdfgen::Vector2 {
                x: 1.0 / shrinkage,
                y: 1.0 / shrinkage,
            },
            translate: msdfgen::Vector2 {
                x: RANGE * shrinkage - bbox.x_min as f64,
                y: RANGE * shrinkage - bbox.y_min as f64,
            },
        },
        range: RANGE * shrinkage,
    };
    let width = ((bbox.x_max as f64 - bbox.x_min as f64) / shrinkage + 2.0 * RANGE).ceil() as u32;
    let height = ((bbox.y_max as f64 - bbox.y_min as f64) / shrinkage + 2.0 * RANGE).ceil() as u32;
    let mut msdf = msdfgen::Bitmap::new(width, height);
    let mut config = msdfgen::MsdfGeneratorConfig::default();
    config.set_mode(msdfgen::ErrorCorrectionMode::Disabled);
    shape.generate_msdf(&mut msdf, framing, config);
    (msdf, shape, framing)
}

pub fn msdfgen_glyph(
    face: &Face,
    glyph_id: GlyphId,
    shrinkage: f64,
) -> msdfgen::Bitmap<msdfgen::Rgb<u8>> {
    msdfgen_glyph_internal(face, glyph_id, shrinkage)
        .0
        .convert()
}

pub fn fdsm_glyph_internal<P: Primitive>(
    face: &Face,
    glyph_id: GlyphId,
    shrinkage: f64,
) -> (
    ImageBuffer<Rgb<P>, Vec<<Rgb<P> as Pixel>::Subpixel>>,
    Shape<ColoredContour>,
    PreparedColoredShape,
)
where
    Rgb<P>: Pixel,
{
    let bbox = face.glyph_bounding_box(glyph_id).unwrap();
    let mut shape = Shape::load_from_face(face, glyph_id);

    const RANGE: f64 = 4.0;
    let transformation = na::convert::<_, Affine2<f64>>(Similarity2::new(
        Vector2::new(
            RANGE - bbox.x_min as f64 / shrinkage,
            RANGE - bbox.y_min as f64 / shrinkage,
        ),
        0.0,
        1.0 / shrinkage,
    ));

    shape.transform(&transformation);

    let colored_shape = Shape::edge_coloring_simple(shape, 0.03, 69441337420);

    let width = ((bbox.x_max as f64 - bbox.x_min as f64) / shrinkage + 2.0 * RANGE).ceil() as u32;
    let height = ((bbox.y_max as f64 - bbox.y_min as f64) / shrinkage + 2.0 * RANGE).ceil() as u32;
    let prepared_shape = colored_shape.prepare();
    let mut msdf = ImageBuffer::new(width, height);
    generate_msdf(&prepared_shape, RANGE, &mut msdf);
    (msdf, colored_shape, prepared_shape)
}

pub fn fdsm_glyph(face: &Face, glyph_id: GlyphId, shrinkage: f64) -> RgbImage {
    fdsm_glyph_internal(face, glyph_id, shrinkage).0
}

#[allow(dead_code)]
pub static INTER: &[u8] = include_bytes!("../assets/Inter-Regular.otf");

include!("../src/_msdf_import_from_ttf_parser.rs");
