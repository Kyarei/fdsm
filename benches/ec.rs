use common::{fdsm_glyph_internal, msdfgen_glyph_internal, INTER};
use criterion::{black_box, criterion_group, criterion_main, BatchSize, Criterion};
use fdsm::correct_error::{correct_error_msdf, ErrorCorrectionConfig};
use ttf_parser::{Face, GlyphId};

mod common;

fn benchmark_error_correction_fdsm(c: &mut Criterion, name: &str, font: &[u8]) {
    let mut group = c.benchmark_group("ec");
    group.sample_size(20);

    let font = Face::parse(font, 0).unwrap();
    let present_glyphs_v = (0..font.number_of_glyphs())
        .filter_map(|i| {
            let glyph_id = GlyphId(i);
            font.glyph_bounding_box(glyph_id)
                .is_some()
                .then(|| fdsm_glyph_internal(&font, glyph_id, 16.0))
        })
        .take(100)
        .collect::<Vec<_>>();
    let present_glyphs = &present_glyphs_v[..];
    group.throughput(criterion::Throughput::Elements(present_glyphs.len() as u64));

    group.bench_function(format!("ec-fdsm {name} 100"), |b| {
        b.iter_batched_ref(
            || {
                present_glyphs
                    .iter()
                    .map(|a| a.0.clone())
                    .collect::<Vec<_>>()
            },
            |msdfs| {
                msdfs
                    .iter_mut()
                    .enumerate()
                    .map(|(i, msdf)| {
                        correct_error_msdf(
                            msdf,
                            &present_glyphs[i].1,
                            &present_glyphs[i].2,
                            black_box(4.0),
                            &ErrorCorrectionConfig::default(),
                        )
                    })
                    .collect::<Vec<_>>()
            },
            BatchSize::SmallInput,
        )
    });

    let present_glyphs_v = (0..font.number_of_glyphs())
        .filter_map(|i| {
            let glyph_id = GlyphId(i);
            font.glyph_bounding_box(glyph_id)
                .is_some()
                .then(|| msdfgen_glyph_internal(&font, glyph_id, 16.0))
        })
        .take(100)
        .collect::<Vec<_>>();
    let present_glyphs = &present_glyphs_v[..];
    group.throughput(criterion::Throughput::Elements(present_glyphs.len() as u64));

    group.bench_function(format!("ec-msdfgen {name} 100"), |b| {
        b.iter_batched_ref(
            || {
                present_glyphs
                    .iter()
                    .map(|a| a.0.clone())
                    .collect::<Vec<_>>()
            },
            |msdfs| {
                msdfs
                    .iter_mut()
                    .enumerate()
                    .map(|(i, msdf)| {
                        present_glyphs[i].1.correct_msdf_error(
                            msdf,
                            present_glyphs[i].2,
                            msdfgen::MsdfGeneratorConfig::default(),
                        )
                    })
                    .collect::<Vec<_>>()
            },
            BatchSize::SmallInput,
        )
    });
}

fn benchmark_ec_noto(c: &mut Criterion) {
    benchmark_error_correction_fdsm(c, "noto", notosans::REGULAR_TTF);
}

fn benchmark_ec_inter(c: &mut Criterion) {
    benchmark_error_correction_fdsm(c, "inter", INTER);
}

criterion_group!(benches, benchmark_ec_noto, benchmark_ec_inter);
criterion_main!(benches);
