use common::INTER;
use criterion::{black_box, criterion_group, criterion_main, Criterion};
use ttf_parser::{Face, GlyphId};

mod common;

fn benchmark_fdsm(c: &mut Criterion, name: &str, font: &[u8]) {
    let mut group = c.benchmark_group("fdsm");
    group.sample_size(10);

    let font = Face::parse(font, 0).unwrap();
    let present_ids_v = (0..font.number_of_glyphs())
        .filter_map(|i| {
            let glyph_id = GlyphId(i);
            font.glyph_bounding_box(glyph_id)
                .is_some()
                .then_some(glyph_id)
        })
        .take(100)
        .collect::<Vec<_>>();
    let present_ids = &present_ids_v[..];
    group.throughput(criterion::Throughput::Elements(present_ids.len() as u64));

    for shrinkage in [64.0, 32.0, 16.0] {
        group.bench_function(format!("fdsm {name} 100 s{shrinkage}"), |b| {
            b.iter_with_large_drop(|| {
                present_ids
                    .iter()
                    .map(|id| common::fdsm_glyph(&font, black_box(*id), shrinkage))
                    .collect::<Vec<_>>()
            })
        });
        group.bench_function(format!("msdfgen {name} 100 s{shrinkage}"), |b| {
            b.iter_with_large_drop(|| {
                present_ids
                    .iter()
                    .map(|id| common::msdfgen_glyph(&font, black_box(*id), shrinkage))
                    .collect::<Vec<_>>()
            })
        });
    }
    group.finish();
}

fn benchmark_fdsm_noto(c: &mut Criterion) {
    benchmark_fdsm(c, "noto", notosans::REGULAR_TTF);
}

fn benchmark_fdsm_inter(c: &mut Criterion) {
    benchmark_fdsm(c, "inter", INTER);
}

criterion_group!(benches, benchmark_fdsm_noto, benchmark_fdsm_inter);
criterion_main!(benches);
